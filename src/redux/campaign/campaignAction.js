import { api } from '../../api'

const campaignAction = {
  GET_DATA: 'GET_CAMPAIGN',
  SORT_CAMPAIGN_DATA: 'SORT_CAMPAIGN_DATA',
  SET_LOADING_START: 'SET_LOADING_START',
  SET_LOADING_END: 'SET_LOADING_END',

  setData: (data) => {
    return {
      type: "GET_CAMPAIGN",
      payload: data
    }
  },

  sortData: (data) => {
    return {
      type: "SORT_CAMPAIGN_DATA",
      payload: data
    }
  },

  setLoadingStart: () => {
    return {
      type: "SET_LOADING_START",
    }
  },

  setLoadingEnd: () => {
    return {
      type: "SET_LOADING_END",
    }
  },

  /* ##################################################### */

  filterData: (data) => {
    return async (dispatch) => {
      const sortedData = data.sort(function (a, b) {
        return a.donation_received - b.donation_received;
      });
      dispatch(campaignAction.setLoadingStart())
      await dispatch(
        campaignAction.sortData(sortedData)
      )
      dispatch(campaignAction.setLoadingEnd())
    }
  },

  getData: () => {
    return async (dispatch) => {
      try {
        dispatch(campaignAction.setLoadingStart())
        const req = await api.get('/southern-waters-642.appspot.com/fe_code_challenge/campaign.json');
        const res = await req.data
        dispatch(campaignAction.setLoadingEnd())
        await dispatch(campaignAction.setData(res))
      } catch (err) {
        throw err
      }
    }
  },
};

export default campaignAction;