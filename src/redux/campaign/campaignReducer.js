import campaignAction from './campaignAction';

const initState = {
  listCampaign: [],
  loading: true
};

export default function campaignReducer(state = initState, action) {
  switch (action.type) {
    case campaignAction.GET_DATA:
      return {
        ...state,
        listCampaign: action.payload.data,
        loading: false
      };
    case campaignAction.SORT_CAMPAIGN_DATA:
      return {
        ...state,
        listCampaign: action.payload,
        loading: false
      };
    case campaignAction.SET_LOADING_START:
      return {
        ...state,
        loading: true
      };
    case campaignAction.SET_LOADING_END:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
