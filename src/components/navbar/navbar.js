import React from "react";
import { useDispatch } from 'react-redux'
import campaignAction from "../../redux/campaign/campaignAction";

function Navbar(props) {
  const dispatch = useDispatch();
  const { filterData } = campaignAction;
  return (
    <div className="navbar">
      <div className="menu">
        <div className="menu-left">
          <img src="https://www.indorelawan.org/uploads/user_avatar/Kitabisa_20210720_233905.jpg" height="50" width="100" alt="" />
        </div>
        <div className="menu-right">
          <button onClick={() => { dispatch(filterData(props.listCampaign)) }}> sort </button>
        </div>
      </div>
    </div>
  )
}

export default Navbar